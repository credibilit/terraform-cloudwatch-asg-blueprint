variable "account" {
  description = "The AWS account number"
}

variable "account_name" {
  description = "The AWS account name or alias"
}

variable "env" {
  description = "The environment name"
}

variable "asg_name" {
  description = "The AWS ASG name"
}

variable "alarm_actions" {
  type = "list"
  description = "The AWS Cloud Watch alarm actions"
}

// Total Instances Max Size
variable "total_instances_max_size_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "total_instances_max_size_evaluation_periods" {
  default = 1
}

variable "total_instances_max_size_period" {
  default = 300
}

variable "total_instances_max_size_statistic" {
  default = "Maximum"
}

variable "total_instances_max_size_threshold" {
}

variable "total_instances_max_size_unit" {
  default = ""
}

// In Service
variable "in_service_comparison_operator" {
  default = "LessThanThreshold"
}

variable "in_service_evaluation_periods" {
  default = 3
}

variable "in_service_period" {
  default = 300
}

variable "in_service_statistic" {
  default = "Maximum"
}

variable "in_service_threshold" {
}

variable "in_service_unit" {
  default = ""
}

// Terminating Instances
variable "terminating_instances_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "terminating_instances_evaluation_periods" {
  default = 3
}

variable "terminating_instances_period" {
  default = 300
}

variable "terminating_instances_statistic" {
  default = "Sum"
}

variable "terminating_instances_threshold" {
  default = 2
}

variable "terminating_instances_unit" {
  default = ""
}
