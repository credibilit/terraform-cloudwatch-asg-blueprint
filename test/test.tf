// VPC
module "test_vpc" {
  source  = "git::https://bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.8"
  account = "${var.account}"

  name                      = "cloudwatch_test_asg"
  domain_name               = "cloudwatch_test_asg.local"
  cidr_block                = "10.0.0.0/16"
  azs                       = "${data.aws_availability_zones.azs.names}"
  az_count                  = 4
  hosted_zone_comment       = "An internal hosted zone for testing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
}

// ASG
module "asg" {
  source  = "git::https://bitbucket.org/credibilit/terraform-autoscaling-blueprint.git?ref=1.1.2"
  account = "${var.account}"

  name                    = "TestCloudWatchASG"
  name_prefix             = "lc"
  max_size                = 2
  min_size                = 1
  launchs_configurations  = [
    {
      name_suffix = "test-cloudwatch-asg"
      image_id    = "ami-4f0c4758"
    }
  ]
  launchs_configurations_count  = 1
  launch_configuration_index    = 0
  vpc_zone_identifier           = "${module.test_vpc.public_subnets}"
}

// Cloud Watch Alarm Action
resource "aws_sns_topic" "monitoring" {
  name         = "monitoring"
  display_name = "monitoring"
}

// Cloud Watch Alarm
module "test" {
  source  = "../"
  account = "${var.account}"

  account_name  = "credibilit-tst"
  env           = "tst"
  asg_name      = "TestCloudWatchASG"
  alarm_actions = ["${aws_sns_topic.monitoring.arn}"]

  total_instances_max_size_threshold  = 1 // == max size
  in_service_threshold                = 10 // == min size
}
