# Terminating Instances
resource "aws_cloudwatch_metric_alarm" "terminating_instances" {
  alarm_name          = "[${var.account_name}] [asg] [${var.env}] ${var.asg_name} - Auto Scaling Group Termination Instances"
  comparison_operator = "${var.terminating_instances_comparison_operator}"
  evaluation_periods  = "${var.terminating_instances_evaluation_periods}"
  metric_name         = "GroupTerminatingInstances"
  namespace           = "AWS/AutoScaling"
  period              = "${var.terminating_instances_period}"
  statistic           = "${var.terminating_instances_statistic}"
  threshold           = "${var.terminating_instances_threshold}"
  unit                = "${var.terminating_instances_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    AutoScalingGroupName = "${var.asg_name}"
  }
}

output "terminating_instances" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.terminating_instances.id}"
  }
}
