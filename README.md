# Cloud Watch Alerts

```
module "<MODULE_NAME>" {
  source  = "../"
  account = "${var.account}"

  account_name  = "credibilit-tst"
  env           = "tst"
  asg_name      = "Test_CloudWatch"
  alarm_actions = ["${aws_sns_topic.monitoring.arn}"]

  // Total Instances Max Size
  total_instances_max_size_comparison_operator = "GreaterThanOrEqualToThreshold"
  total_instances_max_size_evaluation_periods  = 1
  total_instances_max_size_period              = 300
  total_instances_max_size_statistic           = "Maximum"
  total_instances_max_size_threshold           = 10
  total_instances_max_size_unit                = ""

  // In Service
  in_service_comparison_operator = "LessThanThreshold"
  in_service_evaluation_periods  = 3
  in_service_period              = 300
  in_service_statistic           = "Maximum"
  in_service_threshold           = 2
  in_service_unit                = ""

  // Terminating Instances
  terminating_instances_comparison_operator = "GreaterThanOrEqualToThreshold"
  terminating_instances_evaluation_periods  = 3
  terminating_instances_period              = 300
  terminating_instances_statistic           = "Maximum"
  terminating_instances_threshold           = 2
  terminating_instances_unit                = ""
}
```
