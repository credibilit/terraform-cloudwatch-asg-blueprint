# Total Instances Max Size
resource "aws_cloudwatch_metric_alarm" "total_instances_max_size" {
  alarm_name          = "[${var.account_name}] [asg] [${var.env}] ${var.asg_name} - Auto Scaling Group reach Total Instances Max Size Value"
  comparison_operator = "${var.total_instances_max_size_comparison_operator}"
  evaluation_periods  = "${var.total_instances_max_size_evaluation_periods}"
  metric_name         = "GroupTotalInstances"
  namespace           = "AWS/AutoScaling"
  period              = "${var.total_instances_max_size_period}"
  statistic           = "${var.total_instances_max_size_statistic}"
  threshold           = "${var.total_instances_max_size_threshold}"
  unit                = "${var.total_instances_max_size_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    AutoScalingGroupName = "${var.asg_name}"
  }
}

output "total_instances_max_size" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.total_instances_max_size.id}"
  }
}
