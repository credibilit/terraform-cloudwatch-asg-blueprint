# In Service
resource "aws_cloudwatch_metric_alarm" "in_service" {
  alarm_name          = "[${var.account_name}] [asg] [${var.env}] ${var.asg_name} - Auto Scaling Group In Service Instances"
  comparison_operator = "${var.in_service_comparison_operator}"
  evaluation_periods  = "${var.in_service_evaluation_periods}"
  metric_name         = "GroupInServiceInstances"
  namespace           = "AWS/AutoScaling"
  period              = "${var.in_service_period}"
  statistic           = "${var.in_service_statistic}"
  threshold           = "${var.in_service_threshold}"
  unit                = "${var.in_service_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    AutoScalingGroupName = "${var.asg_name}"
  }
}

output "in_service" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.in_service.id}"
  }
}
